package parser

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"strings"

	yaml "gopkg.in/yaml.v3"
)

func ParseYaml(file string, outfile string, envs map[string]string) (string, error) {
	if file == "" {
		fmt.Println(errors.New("No file provided"))
		os.Exit(1)
	}
	content, err := readYaml(file)
	if err != nil {
		return "", err
	}
	for key, value := range envs {
		content = bytes.ReplaceAll(
			content,
			[]byte(fmt.Sprintf("${{ %s }}", key)),
			[]byte(value),
		)
	}
	if outfile != "" {
		err = os.WriteFile(outfile, content, 0666)
		return "", err
	}
	return string(content), nil
}

func readYaml(file string) ([]byte, error) {
	file_sections := strings.Split(file, ".")
	file_ext := file_sections[len(file_sections)-1]
	if file_ext != "yml" && file_ext != "yaml" {
		return []byte{}, errors.New("File does not have a yaml extension")
	}
	content, err := os.ReadFile(file)
	if err != nil {
		return []byte{}, err
	}
	data := make(map[interface{}]interface{})
	err = yaml.Unmarshal(content, &data)
	return content, err
}

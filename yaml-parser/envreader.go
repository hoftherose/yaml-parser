package parser

import (
	"errors"
	"os"
	"strings"
)

func ParseEnv(envfile string) (map[string]string, error) {
	if envfile == "" {
		return map[string]string{}, errors.New("No envfile provided")
	}
	content, err := os.ReadFile(envfile)
	if err != nil {
		return map[string]string{}, err
	}
	contentLines := strings.Split(string(content), "\n")
	envs := map[string]string{}
	for i, line := range contentLines {
		values := strings.Split(line, "=")
		if len(values) != 2 {
			return map[string]string{}, errors.New("Incorrect env=var pattern in line " + string(i))
		}
		envs[values[0]] = values[1]
	}
	return envs, nil
}

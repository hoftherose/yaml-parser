package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"strings"

	parser "gitlab.com/hoftherose/yaml-parser/yaml-parser"
)

type EnvFlags map[string]string

func (i *EnvFlags) String() string {
	return "Environment variables to be swapped"
}

func (i *EnvFlags) Set(value string) error {
	split_value := strings.Split(value, "=")
	if len(split_value) != 2 {
		return errors.New("Could not correctly parse variable, please ensure that format is env=var")
	}
	(*i)[split_value[0]] = split_value[1]
	return nil
}

func (i *EnvFlags) Size() int {
	return len(*i)
}

func main() {
	var envFlags EnvFlags = make(EnvFlags, 10)
	replaceCmd := flag.NewFlagSet("replace", flag.ExitOnError)
	filePtr := replaceCmd.String("file", "", "Template file used to inject values.")
	outputPtr := replaceCmd.String("output", "", "Output file with new values, if not assigned will return as output.")
	replaceCmd.Var(&envFlags, "envs", "Some description for this param.")

	varfileCmd := flag.NewFlagSet("replace", flag.ExitOnError)
	file2Ptr := varfileCmd.String("file", "", "Template file used to inject values.")
	output2Ptr := varfileCmd.String("output", "", "Output file with new values, if not assigned will return as output.")
	envfilePtr := varfileCmd.String("envfile", "", "File with environment variables in .env file format.")

	if len(os.Args) < 2 {
		fmt.Println("expected 'replace' or 'varfile'")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "replace":
		replaceCmd.Parse(os.Args[2:])
		resp, err := parser.ParseYaml(*filePtr, *outputPtr, envFlags)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if resp != "" {
			fmt.Println(resp)
		}
	case "varfile":
		varfileCmd.Parse(os.Args[2:])
		envs, err := parser.ParseEnv(*envfilePtr)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		resp, err := parser.ParseYaml(*file2Ptr, *output2Ptr, envs)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if resp != "" {
			fmt.Println(resp)
		}
	default:
		fmt.Println("expected 'replace' or 'varfile'")
		os.Exit(1)
	}
	os.Exit(0)
}
